#include <Arduino.h>
#include "User_Setup.h"

namespace {
    int switchState = 0;
    const uint8_t BUTTON = 2;
    const uint8_t GREEN_LED = 3;
    const uint8_t RED_LED_1 = 4;
    const uint8_t RED_LED_2 = 5;
}

void setup() {
    pinMode(BUTTON, INPUT);
    pinMode(GREEN_LED, OUTPUT);
    pinMode(RED_LED_1, OUTPUT);
    pinMode(RED_LED_2, OUTPUT);
}

void loop() {
    switchState = digitalRead(BUTTON);

    if (switchState == LOW) {
        digitalWrite(GREEN_LED, HIGH);
        digitalWrite(RED_LED_1, LOW);
        digitalWrite(RED_LED_2, LOW);
    } else {
        digitalWrite(GREEN_LED, LOW);
        digitalWrite(RED_LED_1, LOW);
        digitalWrite(RED_LED_2, HIGH);
        delay(250);

        digitalWrite(GREEN_LED, LOW);
        digitalWrite(RED_LED_1, HIGH);
        digitalWrite(RED_LED_2, LOW);
        delay(250);
    }
}
