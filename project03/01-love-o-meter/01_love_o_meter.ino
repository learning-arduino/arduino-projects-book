#include <Arduino.h>
#include "User_Setup.h"

namespace {
    const int sensorPin = A0;
    const uint8_t ledPin1 = 2;
    const uint8_t ledPin2 = 3;
    const uint8_t ledPin3 = 4;
    const float baselineTemp = 24.0;
}

void setup() {
    Serial.begin(9600);
    pinMode(ledPin1, OUTPUT);
    digitalWrite(ledPin1, LOW);
    pinMode(ledPin2, OUTPUT);
    digitalWrite(ledPin2, LOW);
    pinMode(ledPin3, OUTPUT);
    digitalWrite(ledPin3, LOW);
}

void loop() {
    int sensorVal = analogRead(sensorPin);
    Serial.print("Sensor value: ");
    Serial.print(sensorVal);

    float voltage = (sensorVal / 1024.0) * 5.0;
    Serial.print(", Volts: ");
    Serial.print(voltage);

    float temperature = (voltage - .5) * 100;
    Serial.print(", degrees c: ");
    Serial.println(temperature);

    if (temperature < baselineTemp) {
        digitalWrite(ledPin1, LOW);
        digitalWrite(ledPin2, LOW);
        digitalWrite(ledPin3, LOW);
    } else if (temperature >= baselineTemp + 2 && temperature < baselineTemp + 4) {
        digitalWrite(ledPin1, HIGH);
        digitalWrite(ledPin2, LOW);
        digitalWrite(ledPin3, LOW);
    } else if (temperature >= baselineTemp + 4 && temperature < baselineTemp + 6) {
        digitalWrite(ledPin1, HIGH);
        digitalWrite(ledPin2, HIGH);
        digitalWrite(ledPin3, LOW);
    } else if (temperature >= baselineTemp + 6) {
        digitalWrite(ledPin1, HIGH);
        digitalWrite(ledPin2, HIGH);
        digitalWrite(ledPin3, HIGH);
    }
    delay(300);
}
